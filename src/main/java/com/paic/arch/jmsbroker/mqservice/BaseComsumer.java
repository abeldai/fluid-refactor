package com.paic.arch.jmsbroker.mqservice;


/**
 * @author: daizhong
 * @createTime: 18/2/4
 */
public interface BaseComsumer {
    String getMsg(String topic);
}
