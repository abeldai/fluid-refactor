package com.paic.arch.jmsbroker.mqservice.mqBrokers;

import com.paic.arch.jmsbroker.mqservice.BaseComsumer;
import com.paic.arch.jmsbroker.mqservice.BaseProducer;

/**
 * @author: daizhong
 * @createTime: 18/2/5
 */
public class RocketMqBroker implements BaseProducer,BaseComsumer {

    @Override
    public boolean sendMsg(String topic,String msg){
        return true;
    }

    @Override
    public String getMsg(String topic){
        return "";
    }
}
