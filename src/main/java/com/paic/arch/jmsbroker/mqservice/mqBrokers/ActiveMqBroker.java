package com.paic.arch.jmsbroker.mqservice.mqBrokers;

import com.paic.arch.jmsbroker.JmsMessageBrokerSupport;
import com.paic.arch.jmsbroker.mqservice.BaseComsumer;
import com.paic.arch.jmsbroker.mqservice.BaseProducer;
import org.apache.activemq.ActiveMQConnectionFactory;
import org.slf4j.Logger;

import javax.jms.*;

import java.lang.IllegalStateException;

import static org.slf4j.LoggerFactory.getLogger;

/**
 * @author: daizhong
 * @createTime: 18/2/5
 */
public class ActiveMqBroker implements BaseProducer,BaseComsumer {
    private static final Logger LOG = getLogger(ActiveMqBroker.class);
    private String brokerUrl;
    private Connection connection;
    private static Long aTimeout=2000L;
    public ActiveMqBroker(String brokerUrl){
        this.brokerUrl=brokerUrl;
        initConnect();
    }

    public void initConnect(){
        if(connection==null){
            synchronized (this){
                try{
                    ActiveMQConnectionFactory connectionFactory = new ActiveMQConnectionFactory(brokerUrl);
                    connection = connectionFactory.createConnection();
                    connection.start();
                }catch (Exception e){
                    LOG.error("Failed to create connection ",e);
                }
            }
        }
    }

    @Override
    public boolean sendMsg(String topic,String msg){
        Session session = null;
        try {
            session = connection.createSession(false, Session.AUTO_ACKNOWLEDGE);
            Queue queue = session.createQueue(topic);
            MessageProducer producer = session.createProducer(queue);
            producer.send(session.createTextMessage(msg));
            producer.close();
            LOG.debug("消息发送成功,topic:{},msg:{}",topic,msg);
            return true;
        } catch (JMSException jmse) {
            LOG.error("Failed to create session ", connection,session);
        } finally {
            if (session != null) {
                try {
                    session.close();
                } catch (JMSException jmse) {
                    LOG.warn("Failed to close session {}", session);
                }
            }
        }
        return false;
    }

    @Override
    public String getMsg(String topic){
        Session session = null;
        try {
            session = connection.createSession(false, Session.AUTO_ACKNOWLEDGE);
            Queue queue = session.createQueue(topic);
            MessageConsumer consumer = session.createConsumer(queue);
            Message message = consumer.receive(aTimeout);
            if (message == null) {
                throw new RuntimeException(String.format("No messages received from the broker within the %d timeout", aTimeout));
            }
            consumer.close();
            String result=((TextMessage) message).getText();
            LOG.debug("消息消费成功,topic:{},msg:{}",topic,result);
            return result;
        } catch (JMSException jmse) {
            LOG.error("Failed to create session on connection {}", connection);
            throw new IllegalStateException(jmse);
        } finally {
            if (session != null) {
                try {
                    session.close();
                } catch (JMSException jmse) {
                    LOG.warn("Failed to close session {}", session);
                    throw new RuntimeException(jmse);
                }
            }
        }
    }
}
