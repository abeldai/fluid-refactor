package com.paic.arch.jmsbroker;

import com.paic.arch.jmsbroker.mqservice.mqBrokers.ActiveMqBroker;
import org.junit.AfterClass;
import org.junit.BeforeClass;
import org.junit.Test;

import static org.assertj.core.api.Assertions.assertThat;

public class JmsMessageBrokerSupportTest {

    public static final String TEST_QUEUE = "MY_TEST_QUEUE";
    public static final String MESSAGE_CONTENT = "Lorem blah blah";
    private static JmsMessageBrokerSupport JMS_SUPPORT;
    private static String REMOTE_BROKER_URL;

    @BeforeClass
    public static void setup() throws Exception {
        JMS_SUPPORT = JmsMessageBrokerSupport.createARunningEmbeddedBrokerOnAvailablePort();
        REMOTE_BROKER_URL = JMS_SUPPORT.getBrokerUrl();
    }

    @AfterClass
    public static void teardown() throws Exception {
        JMS_SUPPORT.stopTheRunningBroker();
    }

    @Test
    public void sendsMessagesToTheRunningBroker() throws Exception {
        JmsMessageBrokerSupport.bindToBrokerAtUrl(REMOTE_BROKER_URL)
                .andThen().sendATextMessageToDestinationAt(TEST_QUEUE, MESSAGE_CONTENT);
        long messageCount = JMS_SUPPORT.getEnqueuedMessageCountAt(TEST_QUEUE);
        assertThat(messageCount).isEqualTo(1);
    }

    @Test
    public void readsMessagesPreviouslyWrittenToAQueue() throws Exception {
        String receivedMessage = JmsMessageBrokerSupport.bindToBrokerAtUrl(REMOTE_BROKER_URL)
                .sendATextMessageToDestinationAt(TEST_QUEUE, MESSAGE_CONTENT)
                .andThen().retrieveASingleMessageFromTheDestination(TEST_QUEUE);
        assertThat(receivedMessage).isEqualTo(MESSAGE_CONTENT);
    }

    @Test(expected = JmsMessageBrokerSupport.NoMessageReceivedException.class)
    public void throwsExceptionWhenNoMessagesReceivedInTimeout() throws Exception {
        JmsMessageBrokerSupport.bindToBrokerAtUrl(REMOTE_BROKER_URL).retrieveASingleMessageFromTheDestination(TEST_QUEUE, 1);
    }

    /**
     * 新加测试用例,实现ActiveMqBroker插件的发送和接收
     * 时间有限,旧的api暂时没花时间去重构,一般建议新业务启用新api,旧api逻辑保持正常使用.等有空或者旧api支撑不住了,考虑去重构旧api
     * TODO:将发送者和消费者分成两个类,各自只负责自己的工作
     * TODO:将connect或session建连接池,避免每次使用都重新连接
     */
    @Test
    public void sendMsgToActiveMqBroker(){
        String newBrokerUrl="tcp://localhost:"+SocketFinder.findNextAvailablePortBetween(41616, 50000);
        ActiveMqBroker activeMqBroker=new ActiveMqBroker(newBrokerUrl);
        activeMqBroker.sendMsg(TEST_QUEUE,MESSAGE_CONTENT);
        String receivedMessage=activeMqBroker.getMsg(TEST_QUEUE);
        assertThat(receivedMessage).isEqualTo(MESSAGE_CONTENT);
    }


}